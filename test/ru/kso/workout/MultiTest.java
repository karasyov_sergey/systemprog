package ru.kso.workout;

import org.testng.annotations.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MultiTest {

    @Test
    public void positiveMulti() {
        int firstNum = (int) (Math.random() * 100);
        int secondNum = (int) (Math.random() * 100);
        int result = firstNum * secondNum;
        assertEquals(result, Multi.multi(firstNum, secondNum));
    }

    @Test
    public void negativeMulti() {
        int firstNum = (int) (- 100 + Math.random() * 99);
        int secondNum = (int) (- 100 + Math.random() * 99);
        int result = firstNum * secondNum;
        assertEquals(result, Multi.multi(firstNum, secondNum));
    }
}