package ru.kso.tasks.math;

import org.junit.Test;
import ru.kso.myexceptions.mathexceptions.DivByZeroException;
import ru.kso.myexceptions.mathexceptions.NegativeExpException;
import ru.kso.myexceptions.mathexceptions.TypeOverflowException;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyMathTest {

    @Test
    public void addPositive() {
        int firstNum = (int) (Math.random() * 100);
        int secondNum = (int) (Math.random() * 100);
        int result = firstNum + secondNum;
        try {
            assertEquals(result, MyMath.add(firstNum, secondNum));
        } catch (TypeOverflowException ignored) {

        }
    }

    @Test
    public void addNegative() {
        int firstNum = (int) (-100 + Math.random() * 100);
        int secondNum = (int) (-100 + Math.random() * 100);
        int result = firstNum + secondNum;
        try {
            assertEquals(result, MyMath.add(firstNum, secondNum));
        } catch (TypeOverflowException ignored) {

        }
    }

    @Test
    public void subPositive() {
        int firstNum = (int) (Math.random() * 100);
        int secondNum = (int) (Math.random() * 100);
        int result = firstNum - secondNum;
        try {
            assertEquals(result, MyMath.sub(firstNum, secondNum));
        } catch (TypeOverflowException ignored) {

        }
    }

    @Test
    public void subNegative() {
        int firstNum = (int) (-100 + Math.random() * 100);
        int secondNum = (int) (-100 + Math.random() * 100);
        int result = firstNum - secondNum;
        try {
            assertEquals(result, MyMath.sub(firstNum, secondNum));
        } catch (TypeOverflowException ignored) {

        }
    }

    @Test
    public void divPositive() {
        int firstNum = (int) (Math.random() * 100);
        int secondNum = (int) (Math.random() * 100);
        int result = firstNum / secondNum;
        try {
            assertEquals(result, MyMath.div(firstNum, secondNum));
        } catch (DivByZeroException | TypeOverflowException ignored) {

        }
    }

    @Test
    public void divNegative() {
        int firstNum = (int) (-100 + Math.random() * 100);
        int secondNum = (int) (-100 + Math.random() * 100);
        int result = firstNum / secondNum;
        try {
            assertEquals(result, MyMath.div(firstNum, secondNum));
        } catch (DivByZeroException | TypeOverflowException ignored) {

        }
    }

    @Test
    public void divException() {
        int firstNum = (int) (Math.random() * 100);
        int secondNum = 0;
        try {
            int result = MyMath.div(firstNum, secondNum);
            assertEquals(result, MyMath.div(firstNum, secondNum));
            fail();
        } catch (DivByZeroException | TypeOverflowException ignored) {

        }
    }

    @Test
    public void multiPositive() {
        int firstNum = (int) (Math.random() * 100);
        int secondNum = (int) (Math.random() * 100);
        int result = firstNum * secondNum;
        try {
            assertEquals(result, MyMath.multi(firstNum, secondNum));
        } catch (TypeOverflowException ignored) {

        }
    }

    @Test
    public void multiNegative() {
        int firstNum = (int) (-100 + Math.random() * 100);
        int secondNum = (int) (-100 + Math.random() * 100);
        int result = firstNum * secondNum;
        try {
            assertEquals(result, MyMath.multi(firstNum, secondNum));
        } catch (TypeOverflowException ignored) {

        }
    }

    @Test
    public void positivePow() {
        int firstNum = (int) (Math.random() * 10);
        int secondNum = (int) (Math.random() * 10);
        int result = (int) Math.pow(firstNum, secondNum);
        try {
            assertEquals(result, MyMath.positivePow(firstNum, secondNum));
        } catch (NegativeExpException | TypeOverflowException ignored) {

        }
    }

    @Test
    public void positivePowException() {
        int firstNum = (int) (Math.random() * 100);
        int secondNum = -1;
        double result = Math.pow(firstNum, secondNum);
        try {
            assertEquals(result, MyMath.positivePow(firstNum, secondNum));

        } catch (NegativeExpException | TypeOverflowException ignored) {

        }
    }

    @Test
    public void modPositive() {
        int firstNum = (int) (Math.random() * 100);
        int secondNum = (int) (Math.random() * 100);
        int result = firstNum % secondNum;
        try {
            assertEquals(result, MyMath.mod(firstNum, secondNum));
        } catch (DivByZeroException ignored) {

        }
    }

    @Test
    public void modNegative() {
        int firstNum = (int) (-100 + Math.random() * 100);
        int secondNum = (int) (-100 + Math.random() * 100);
        int result = firstNum % secondNum;
        try {
            assertEquals(result, MyMath.mod(firstNum, secondNum));
        } catch (DivByZeroException ignored) {

        }
    }

    @Test
    public void modException() {
        int firstNum = (int) (Math.random() * 100);
        int secondNum = 0;
        try {
            int result = MyMath.mod(firstNum, secondNum);
            assertEquals(result, MyMath.mod(firstNum, secondNum));
        } catch (DivByZeroException ignored) {

        }
    }
}