package ru.kso.selectors;

import ru.kso.myexceptions.mathexceptions.*;

public class ExceptionMessageSelector {

    private MathParentException e;
    private String message;

    public ExceptionMessageSelector(MathParentException e) {
        this.e = e;
    }

    public void select() {
        switch (e.getClass().getSimpleName()) {
            case "DivByZeroException":
                message = DivByZeroException.EXCEPTION_MESSAGE;
                break;
            case "IncorrectDataException":
                message = IncorrectDataException.EXCEPTION_MESSAGE;
                break;
            case "NegativeExpException":
                message = NegativeExpException.EXCEPTION_MESSAGE;
                break;
            case "TypeOverflowException":
                message = TypeOverflowException.EXCEPTION_MESSAGE;
                break;
        }
    }

    public String getMessage() {
        return message;
    }
}
