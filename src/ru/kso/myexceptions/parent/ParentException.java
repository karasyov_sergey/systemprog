package ru.kso.myexceptions.parent;

import ru.kso.interfaces.exceptionmessage.IMessagePrinter;

public class ParentException extends Exception implements IMessagePrinter {

    @Override
    public void printMessage() {
        System.out.println("Что-то пошло не так :(");
    }
}
