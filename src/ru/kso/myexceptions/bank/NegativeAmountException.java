package ru.kso.myexceptions.bank;

import ru.kso.myexceptions.parent.ParentException;

/**
 *
 */
@SuppressWarnings("unused")
public class NegativeAmountException extends ParentException {

    @Override
    public void printMessage() {
        System.out.println("Невозможно обработать" +
                "отрицательное значение");
    }
}
