package ru.kso.myexceptions.bank;

import ru.kso.myexceptions.parent.ParentException;

@SuppressWarnings("unused")
public class SumExcessException extends ParentException {
    @Override
    public void printMessage() {
        System.out.println("Сумма для снятия" +
                " превышает сумму счета");
    }
}
