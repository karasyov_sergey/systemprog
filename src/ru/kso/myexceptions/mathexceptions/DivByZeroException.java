package ru.kso.myexceptions.mathexceptions;

/**
 * Наследник класса Exception, обрабатывающий исключения типа деление на 0
 */
public class DivByZeroException extends MathParentException {

    public static final String EXCEPTION_MESSAGE = "Деление на 0";

    @Override
    public void printMessage() {
        System.out.println(EXCEPTION_MESSAGE);
    }
}
