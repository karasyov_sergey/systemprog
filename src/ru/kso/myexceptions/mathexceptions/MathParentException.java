package ru.kso.myexceptions.mathexceptions;

import ru.kso.myexceptions.parent.ParentException;

public class MathParentException extends ParentException {

    @Override
    public void printMessage() {
        System.out.println("Ошибка при математических вычислениях");
    }
}
