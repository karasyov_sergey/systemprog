package ru.kso.myexceptions.mathexceptions;

public class IncorrectDataException extends MathParentException {

    public static final String EXCEPTION_MESSAGE = "Некорректно введено выражение";

    @Override
    public void printMessage() {
        System.out.println(EXCEPTION_MESSAGE);
    }
}
