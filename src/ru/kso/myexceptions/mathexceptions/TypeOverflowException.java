package ru.kso.myexceptions.mathexceptions;

/**
 * Наследник класса Exception, который обрабатывает исключения типа переполнение типа данных
 *
 * @author KSO 17ИТ17
 */
public class TypeOverflowException extends MathParentException {

    public static final String EXCEPTION_MESSAGE = "Переполнение";

    @Override
    public void printMessage() {
        System.out.println(EXCEPTION_MESSAGE);
    }
}
