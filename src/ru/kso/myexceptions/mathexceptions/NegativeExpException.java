package ru.kso.myexceptions.mathexceptions;

/**
 * Наследник класса Exception, обрабатывающий исключения типа возведение число в
 * отрицательную степень
 */
public class NegativeExpException extends MathParentException {

    public static final String EXCEPTION_MESSAGE = "Невозможно обработать отрицательную степень числа";

    @Override
    public void printMessage() {
        System.out.println(EXCEPTION_MESSAGE);
    }
}
