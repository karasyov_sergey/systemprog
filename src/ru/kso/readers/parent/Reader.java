package ru.kso.readers.parent;

@SuppressWarnings("unused")
public abstract class Reader {

    private String path;

    public Reader(String path) {
        this.path = path;
    }

    public abstract void toRead();

    protected String getPath() {
        return path;
    }
}
