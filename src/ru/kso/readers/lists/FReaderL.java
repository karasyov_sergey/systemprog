package ru.kso.readers.lists;

import ru.kso.readers.parent.Reader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@SuppressWarnings("unused")

public class FReaderL extends Reader {
    private List<String> listData;

    public FReaderL(String path) {
        super(path);
    }

    public List<String> getListData() {
        return listData;
    }

    private void setListData(List<String> listData) {
        this.listData = listData;
    }

    @Override
    public void toRead() {
        List<String> list;
        try {
            list = Files.readAllLines(Paths.get(getPath()));
            setListData(list);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }
}
