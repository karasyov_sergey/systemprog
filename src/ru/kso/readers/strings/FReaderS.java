package ru.kso.readers.strings;

import ru.kso.readers.parent.Reader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@SuppressWarnings("unused")
public class FReaderS extends Reader {
    private String data;

    public FReaderS(String path) {
        super(path);
    }


    @Override
    public void toRead() {
        StringBuilder txtContent = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(getPath()));
            String line = reader.readLine();
            while (line != null) {
                txtContent.append(line);
                line = reader.readLine();
                txtContent.append("\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }
        String textData = txtContent.toString();
        setData(textData.substring(0, textData.length() - 1));
    }

    private void setData(String data) {
        this.data = data;
    }

    public String getDataText() {
        return data;
    }
}
