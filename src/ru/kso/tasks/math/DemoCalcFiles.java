package ru.kso.tasks.math;

import ru.kso.myexceptions.mathexceptions.*;
import ru.kso.readers.lists.FReaderL;
import ru.kso.selectors.ExceptionMessageSelector;
import ru.kso.writers.arrlists.FWriterAL;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс, демонстрирующий работу исключений и методов класса MyMath путем
 * чтения информации с файла и ее записи на другой файл
 *
 * @author KSO 17ИТ17
 */
public class DemoCalcFiles {

    private static final String WORK_COMPLETE = "Программа завершила работу";

    public static void main(String[] args) {
        try {
            String inputPath = "src/ru/kso/assets/math/input.txt";
            String outputPath = "src/ru/kso/assets/math/output.txt";
            FReaderL reader = new FReaderL(inputPath);
            reader.toRead();
            ArrayList<String> expressions =
                    (ArrayList<String>) reader.getListData();
            ArrayList<String> resultsOfExp = fillByResultsOfCalc(expressions);
            FWriterAL writer = new FWriterAL(outputPath, resultsOfExp);
            writer.write();
        } finally {
            System.out.println(WORK_COMPLETE);
        }
    }


    /**
     * Метод, который записывает в списочный массив результаты работы метода
     * getResultOfCalculate
     *
     * @param expressions списочный массив, содержащий выражения
     * @return списочный массив, содержащий выражения и результаты их вычислений
     */
    private static ArrayList<String> fillByResultsOfCalc(List<String> expressions) {
        ArrayList<String> resultsOfExp = new ArrayList<>();
        String result;
        for (String exp : expressions) {
            try {
                result = String.valueOf(getResultOfCalculate(exp));
                resultsOfExp.add(exp + " = " + result);

            } catch (IncorrectDataException | DivByZeroException |
                    NegativeExpException | TypeOverflowException e) {
                String message = getMessageException(e);
                if (message != null){
                    resultsOfExp.add(message);
                } else {
                    resultsOfExp.add("Unknown error");
                }
            } catch (NumberFormatException e) {
                resultsOfExp.add(TypeOverflowException.EXCEPTION_MESSAGE);
            }
        }
        return resultsOfExp;
    }

    /**
     * Метод, который выбирает сообщение об ошибке в зависимости от пойманного исключения
     *
     * @param e исключение
     * @return сообщение об ошибке
     */
    private static String getMessageException(MathParentException e) {
        ExceptionMessageSelector messageSelector = new ExceptionMessageSelector(e);
        messageSelector.select();
        return messageSelector.getMessage();
    }


    /**
     * Метод, возвращающий результат вычисления выражения
     *
     * @param exp выражение
     * @return результат вычисления выражения
     * @throws DivByZeroException     исключение, выбрасываемое при попытке деления на 0
     * @throws NegativeExpException   исключение, выбрасываемое при попытке
     *                                возведения в отрицательную степень
     * @throws IncorrectDataException исключение, выбрасываемое при некорректно
     *                                введеном выражении
     */
    private static int getResultOfCalculate(String exp) throws DivByZeroException,
            NegativeExpException, IncorrectDataException, TypeOverflowException,
            NumberFormatException {
        if (!exp.trim().matches("[+-]?\\d+ [+\\-*^%/] [+-]?\\d+")) {
            throw new IncorrectDataException();
        }
        String[] elementsOfExp = exp.split(" ");

        int firstNum = Integer.parseInt(elementsOfExp[0]);
        int secondNum = Integer.parseInt(elementsOfExp[2]);
        String sign = elementsOfExp[1];
        return calculate(firstNum, secondNum, sign);

    }

    /**
     * Метод, который рассчитывает результат выражения по знаку операции
     *
     * @param firstNum  первое целое число
     * @param secondNum второе целое число
     * @param sign      знак операции
     * @return результат расчета
     * @throws DivByZeroException   исключение, выбрасываемое при попытке деления на 0
     * @throws NegativeExpException исключение, выбрасываемое при попытке
     *                              возведения в отрицательную степень
     */
    private static int calculate(int firstNum, int secondNum, String sign)
            throws DivByZeroException, NegativeExpException,
            TypeOverflowException {
        int result;
        switch (sign) {
            case "+":
                result = MyMath.add(firstNum, secondNum);
                break;
            case "-":
                result = MyMath.sub(firstNum, secondNum);
                break;
            case "/":
                result = MyMath.div(firstNum, secondNum);
                break;
            case "*":
                result = MyMath.multi(firstNum, secondNum);
                break;
            case "%":
                result = MyMath.mod(firstNum, secondNum);
                break;
            case "^":
                result = MyMath.positivePow(firstNum, secondNum);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + sign);
        }
        return result;
    }

}
