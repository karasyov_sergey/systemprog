package ru.kso.tasks.math;

import ru.kso.myexceptions.mathexceptions.DivByZeroException;
import ru.kso.myexceptions.mathexceptions.NegativeExpException;
import ru.kso.myexceptions.mathexceptions.TypeOverflowException;

/**
 * Класс, содержащий методы, которые производят
 * вычисления типичных математических выражений.
 *
 * @author KSO 17ИТ17
 */
class StrictIntegerMath {
    private static long resultLong;
    private static int resultInt;

    /**
     * Метод, который складывает два числа
     *
     * @param firstNum  первое число
     * @param secondNum второе число
     * @return firstNum + secondNum
     * @throws TypeOverflowException исключение, выбрасываемое при
     *                               переполнении типа данных
     */
    static int add(int firstNum, int secondNum) throws TypeOverflowException {
        resultInt = firstNum + secondNum;
        resultLong = (long) firstNum + secondNum;
        if (resultLong != resultInt) {
            throw new TypeOverflowException();
        }
        return resultInt;
    }

    /**
     * Метод, который вычитает из первого числа второе
     *
     * @param firstNum  первое число
     * @param secondNum второе число
     * @return firstNum - secondNum
     * @throws TypeOverflowException исключение, выбрасываемое при
     *                               переполнении типа данных
     */
    static int sub(int firstNum, int secondNum) throws TypeOverflowException {
        resultInt = firstNum - secondNum;
        resultLong = (long) firstNum - secondNum;
        if (resultInt != resultLong) {
            throw new TypeOverflowException();
        }
        return resultInt;
    }

    /**
     * Метод, который делит первое число на второе
     *
     * @param numerator   числитель
     * @param denominator знаменатель
     * @return numerator / denominator
     * @throws DivByZeroException    исключение, выбрасываемое при попытке
     *                               делить на 0
     * @throws TypeOverflowException сключение, выбрасываемое при
     *                               переполнении типа данных
     */
    static int div(int numerator, int denominator) throws DivByZeroException,
            TypeOverflowException {
        if (denominator == 0) {
            throw new DivByZeroException();
        }
        resultInt = numerator / denominator;
        resultLong = (long) numerator / denominator;
        if (resultInt != resultLong) {
            throw new TypeOverflowException();
        }
        return numerator / denominator;
    }

    /**
     * Метод, который умножает первое число на второе
     *
     * @param firstNum  первое число
     * @param secondNum второе число
     * @return firstNum * secondNum
     * @throws TypeOverflowException исключение, выбрасываемое при
     *                               переполнении типа данных
     */
    static int multi(int firstNum, int secondNum) throws TypeOverflowException {
        resultInt = firstNum / secondNum;
        resultLong = (long) firstNum / secondNum;
        if (resultInt != resultLong) {
            throw new TypeOverflowException();
        }
        return firstNum * secondNum;
    }

    /**
     * Метод, который возводит число в степень
     *
     * @param number   число
     * @param exponent степень
     * @return number<sup>exponent</sup>
     * @throws NegativeExpException  исключение, выбрасываемое при
     *                               попытки возвести в отрицательную степень
     * @throws TypeOverflowException исключение, выбрасываемое при
     *                               переполнении типа данных
     */
    static int positivePow(int number, int exponent) throws NegativeExpException,
            TypeOverflowException {
        if (exponent < 0) {
            throw new NegativeExpException();
        }
        if (exponent == 0) {
            return 1;
        }
        if (exponent == 1){
            return number;
        }
        resultInt = 1;
        resultLong = 1;
        for (int index = 0; index < exponent; index++) {
            resultInt *= number;
            resultLong *= number;
        }
        if (resultInt != resultLong) {
            throw new TypeOverflowException();
        }
        return resultInt;
    }

    /**
     * Метод, который возвращает остаток при делении
     * одно числа на другое
     *
     * @param numerator   числитель
     * @param denominator знаменатель
     * @return numerator % denominator
     * @throws DivByZeroException    исключение, выбрасываемое при попытке
     *                               делить на 0
     */
    static int mod(int numerator, int denominator)
            throws DivByZeroException {
        if (denominator == 0) {
            throw new DivByZeroException();
        }
        return numerator % denominator;
    }
}
