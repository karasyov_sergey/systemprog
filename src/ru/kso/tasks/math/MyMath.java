package ru.kso.tasks.math;

import ru.kso.myexceptions.mathexceptions.DivByZeroException;
import ru.kso.myexceptions.mathexceptions.NegativeExpException;
import ru.kso.myexceptions.mathexceptions.TypeOverflowException;

/**
 * Класс, содержащий методы, возвращающие значения
 * вычислений типичных математических выражений
 *
 * @author KSO 17ИТ17
 */
@SuppressWarnings("all")
public class MyMath {

    /**
     * Метод, возвращающий сумму двух чисел
     *
     * @param firstNum  первое число
     * @param secondNum второе число
     * @return сумма двух чисел
     * @throws TypeOverflowException исключение, выбрасываемое при
     *                               переполнении типа данных
     */
    public static int add(int firstNum, int secondNum)
            throws TypeOverflowException {
        return StrictIntegerMath.add(firstNum, secondNum);
    }

    /**
     * Метод, возвращающий разность двух чисел
     *
     * @param firstNum  первое число
     * @param secondNum второе число
     * @return разность двух чисел
     * @throws TypeOverflowException исключение, выбрасываемое при
     *                               переполнении типа данных
     */
    public static int sub(int firstNum, int secondNum)
            throws TypeOverflowException {
        return StrictIntegerMath.sub(firstNum, secondNum);
    }

    /**
     * Метод, возвращающий частность двух чисел
     *
     * @param numerator   делимое
     * @param denominator делитель
     * @return частное двух чисел
     * @throws DivByZeroException    исключение, выбрасываемое
     *                               при попытке деления на 0
     * @throws TypeOverflowException исключение, выбрасываемое при
     *                               переполнении типа данных
     */
    public static int div(int numerator, int denominator) throws
            DivByZeroException, TypeOverflowException {
        return StrictIntegerMath.div(numerator, denominator);
    }

    /**
     * Метод, рвозвращающий произведение двух чисел
     *
     * @param firstNum  первое число
     * @param secondNum второе число
     * @return результат умножения чисел
     * @throws TypeOverflowException исключение, выбрасываемое при
     *                               переполнении типа данных
     */
    public static int multi(int firstNum, int secondNum)
            throws TypeOverflowException {
        return StrictIntegerMath.multi(firstNum, secondNum);
    }

    /**
     * Метод, возвращающий число, возведенное в степень
     *
     * @param number   число
     * @param exponent степень
     * @return number<sup>exponent</sup>
     * @throws NegativeExpException  исключение, выбрасываемое при
     *                               возведении в отрицательную степень
     * @throws TypeOverflowException исключение, выбрасываемое при
     *                               переполнении типа данных
     */
    public static int positivePow(int number, int exponent)
            throws NegativeExpException, TypeOverflowException {
        return StrictIntegerMath.positivePow(number, exponent);
    }

    /**
     * Метод, возвращающий остаток при делении двух чисел
     *
     * @param numerator   делимое
     * @param denominator делитель
     * @return остаток дробного числа
     * @throws DivByZeroException исключение, выбрасываемое при попытке деления на 0
     */
    public static int mod(int numerator, int denominator)
            throws DivByZeroException {
        return StrictIntegerMath.mod(numerator, denominator);
    }
}
