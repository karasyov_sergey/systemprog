package ru.kso.tasks.math;

import ru.kso.myexceptions.mathexceptions.DivByZeroException;
import ru.kso.myexceptions.mathexceptions.IncorrectDataException;
import ru.kso.myexceptions.mathexceptions.NegativeExpException;
import ru.kso.myexceptions.mathexceptions.TypeOverflowException;
import ru.kso.scanner.Scanners;
import ru.kso.selectors.ExceptionMessageSelector;

/**
 * Класс, демонстрирующий работу исключений и методов класса MyMath путем
 * консольного ввода данных
 *
 * @author KSO 17ИТ17
 */
public class DemoCalcConsole {
    public static void main(String[] args) {
        System.out.print("Введите выражение, содержащее только целые числа " +
                "(пример - 2 + 2): ");
        String expression = Scanners.scannerNextLine();
        try {
            int resultOfCalc = getResultOfCalculate(expression);
            System.out.println(expression.trim() + " = " + resultOfCalc);
        } catch (IncorrectDataException | DivByZeroException |
                NegativeExpException | TypeOverflowException e) {
            ExceptionMessageSelector messageSelector = new ExceptionMessageSelector(e);
            messageSelector.select();
            String message = messageSelector.getMessage();
            if (message != null){
                System.out.println(message);
            }
        } catch (NumberFormatException e) {
            new TypeOverflowException().printMessage();
        } finally {
            System.out.println("Программа завершила работу");
        }
    }


    /**
     * Метод, возвращающий результат вычисления выражения
     *
     * @param exp выражение
     * @return результат вычисления выражения
     * @throws DivByZeroException     исключение, выбрасываемое при попытке деления на 0
     * @throws NegativeExpException   исключение, выбрасываемое при попытке
     *                                возведения в отрицательную степень
     * @throws IncorrectDataException исключение, выбрасываемое при некорректно
     *                                введеном выражении
     *
     */
    private static int getResultOfCalculate(String exp) throws DivByZeroException,
            NegativeExpException, IncorrectDataException, TypeOverflowException,
            NumberFormatException {
        if (!exp.trim().matches("[+-]?\\d+ [+\\-*^%/] [+-]?\\d+")) {
            throw new IncorrectDataException();
        }
        String[] elementsOfExp = exp.split(" ");

        int firstNum = Integer.parseInt(elementsOfExp[0]);
        int secondNum = Integer.parseInt(elementsOfExp[2]);
        String sign = elementsOfExp[1];
        return calculate(firstNum, secondNum, sign);

    }

    /**
     * Метод, который рассчитывает результат выражения по знаку операции
     *
     * @param firstNum  первое целое число
     * @param secondNum второе целое число
     * @param sign      знак операции
     * @return результат расчета
     * @throws DivByZeroException   исключение, выбрасываемое при попытке деления на 0
     * @throws NegativeExpException исключение, выбрасываемое при попытке
     *                              возведения в отрицательную степень
     */
    private static int calculate(int firstNum, int secondNum, String sign)
            throws DivByZeroException, NegativeExpException,
            TypeOverflowException {
        int result;
        switch (sign) {
            case "+":
                result = MyMath.add(firstNum, secondNum);
                break;
            case "-":
                result = MyMath.sub(firstNum, secondNum);
                break;
            case "/":
                result = MyMath.div(firstNum, secondNum);
                break;
            case "*":
                result = MyMath.multi(firstNum, secondNum);
                break;
            case "%":
                result = MyMath.mod(firstNum, secondNum);
                break;
            case "^":
                result = MyMath.positivePow(firstNum, secondNum);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + sign);
        }
        return result;
    }
}