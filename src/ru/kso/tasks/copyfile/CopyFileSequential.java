package ru.kso.tasks.copyfile;

import ru.kso.readers.strings.FReaderS;
import ru.kso.writers.strings.FWriterS;

import java.util.concurrent.TimeUnit;

/**
 * Класс, который последовательно копирует файл в два других
 *
 * @author KSO 17ИТ17
 */
public class CopyFileSequential {
    public static void main(String[] args) {
        TimeUnit timeUnit = TimeUnit.NANOSECONDS;
        final long beginning = System.nanoTime();
        FReaderS readerFromFile = new FReaderS("src/ru/kso/assets/copyfile/input.txt");
        readerFromFile.toRead();
        String data = readerFromFile.getDataText();
        System.out.printf("Время чтения = %d%n", timeUnit.toMillis(System.nanoTime() - beginning));

        final long startOne = System.nanoTime();
        FWriterS writerToFile = new FWriterS("src/ru/kso/assets/copyfile/output.txt", data);
        writerToFile.write();
        System.out.printf("Время записи 1 = %d%n", timeUnit.toMillis(System.nanoTime() - startOne));

        final long startTwo = System.nanoTime();
        FWriterS writerToFileTwo = new FWriterS("src/ru/kso/assets/copyfile/outputTwo.txt", data);
        writerToFileTwo.write();
        System.out.printf("Время записи 2 = %d%n", timeUnit.toMillis(System.nanoTime() - startTwo));
        System.out.printf("Время работы программы = %d", timeUnit.toMillis(System.nanoTime() - beginning));
    }
}
