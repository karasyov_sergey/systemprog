package ru.kso.tasks.copyfile;

import ru.kso.threads.ThreadReadFile;
import ru.kso.threads.ThreadWriteFile;

import java.util.concurrent.TimeUnit;

/**
 * Класс, который параллельно копирует файл в два других
 *
 * @author KSO 17ИТ17
 */
public class CopyFileParallel {

    public static void main(String[] args) {
        TimeUnit timeUnit = TimeUnit.NANOSECONDS;
        final long beginning = System.nanoTime();

        String pathInput = "src/ru/kso/assets/copyfile/input.txt";
        ThreadReadFile threadReadFile = new ThreadReadFile(pathInput);
        threadReadFile.start();
        try {
            threadReadFile.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String data = threadReadFile.getData();
        String pathOutput = "src/ru/kso/assets/copyfile/output.txt";
        String pathOutputTwo = "src/ru/kso/assets/copyfile/outputTwo.txt";
        ThreadWriteFile threadWriteFile = new ThreadWriteFile(pathOutput, data);
        ThreadWriteFile threadWriteFileTwo = new ThreadWriteFile(pathOutputTwo, data);
        threadWriteFile.start();
        threadWriteFileTwo.start();

        final long end = System.nanoTime();
        System.out.printf("Время главного потока main = %d%n", timeUnit.toMillis(end - beginning));
    }
}