package ru.kso.tasks.chickenoredge;

import ru.kso.threads.MyThread;

/**
 * Класс, который определяет, что появилось раньше -
 * курица или яйцо?
 *
 * @author KSO 17ИТ17
 */
public class ChickenOrEdge {
    public static void main(String[] args) {
        MyThread chicken = new MyThread("курица");
        MyThread egg = new MyThread("яйцо");
        egg.start();
        chicken.start();
        try {
            egg.join();
            if (!chicken.isAlive()) {
                System.out.println("Яйцо победило");
            } else {
                chicken.join();
                System.out.println("Курица победила");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}