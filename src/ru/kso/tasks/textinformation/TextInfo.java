package ru.kso.tasks.textinformation;

import ru.kso.scanner.Scanners;
import ru.kso.readers.strings.FReaderS;
import ru.kso.writers.strings.FWriterS;

/**
 * Класс, расчитывающий количество символов в тексте,
 * символов в тексте без пробелов и количество слов. Данные получаются из
 * файла, который содержит текст.
 *
 * @author KSO 17ИТ17
 */
public class TextInfo {

    private static final String QUANTITY_OF_SYMBOLS = "Количество символов = ";
    private static final String QUANTITY_OF_SYMBOLS_WITHOUT_SPACE = "Количество символов без пробелов = ";
    private static final String QUANTITY_OF_WORDS = "Количество слов = ";

    public static void main(String[] args) {
        System.out.print("Введите путь к исходному файлу: ");
        String pathInput = Scanners.scannerNextLine();
        FReaderS reader = new FReaderS(pathInput);
        reader.toRead();
        final String textContent = reader.getDataText();
        int quantityOfSymbols = getQuantityOfSymbols(textContent);
        int quantityOfSpaces = calcSpaces(textContent);
        int quantityOfWords = calcWords(textContent);
        String infoMessage = generateText(QUANTITY_OF_SYMBOLS +
                        quantityOfSymbols, QUANTITY_OF_SYMBOLS_WITHOUT_SPACE +
                        (quantityOfSymbols - quantityOfSpaces),
                QUANTITY_OF_WORDS + quantityOfWords);
        System.out.print("Введите путь к файлу для записи результата: ");
        String pathOutput = Scanners.scannerNextLine();
        FWriterS writer = new FWriterS(pathOutput, infoMessage);
        writer.write();
        System.out.println(infoMessage);

    }

    /**
     * Метод, позволяющий получить количество символов в тексте
     *
     * @param str строка, содержащая текст
     * @return количество символов в тексте
     */
    private static int getQuantityOfSymbols(String str) {
        return str.replaceAll("\\n", "").length();
    }

    /**
     * Метод, возвращающий количество пробелов в тексте
     *
     * @param str строка, содержащая текст
     * @return количество пробелов в тексте
     */
    private static int calcSpaces(String str) {
        return str.replaceAll("\\S|\\n", "").length();
    }

    /**
     * Метод, возвращающий количество слов, содержащиеся в тексте
     *
     * @param str строка, содержащая текст
     * @return количество слов, которые содержатся в тексте
     */
    private static int calcWords(String str) {
        int quantity = 0;
        String[] arrayOfText = str.split("\\s");
        for (String data : arrayOfText) {
            if (data.replaceAll("\\W|_", "").
                    matches("\\w+|\\w+[-]?\\w+")) {
                quantity++;
            }
            if (data.replaceAll("\\W[^а-яА-Я]|_", "").
                    matches("[а-яА-Я]+|[а-яА-Я]+[-]?[а-яА-Я]+")) {
                quantity++;
            }
        }
        return quantity;
    }

    /**
     * Метод, Формирующмй строку из указанных аргументов. После каждого
     * элмента строковых аргументов добавляется новая строка, кроме
     * последнего элемента
     *
     * @param args передаваемые n количество строк
     * @return строка, состоящая из полученных извне аргументов
     */
    private static String generateText(String... args) {
        StringBuilder generatingText = new StringBuilder();
        for (int i = 0; i < args.length; i++) {
            generatingText.append(args[i]);
            if (i != args.length - 1) {
                generatingText.append("\n");
            }
        }
        return generatingText.toString();
    }
}