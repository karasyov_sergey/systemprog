package ru.kso.tasks.niocopy.sequential;

import ru.kso.threads.ThreadNIOCopy;

import java.util.concurrent.TimeUnit;

/**
 * Класс, последовательно копирующий один файл в два других.
 * Для работы необходимо указать программные аргументы
 *
 * @author KSO 17ИТ17
 */
@SuppressWarnings("all")
public class NIOCopyS {

    public static void main(String[] args) {
        if (args.length != 3){
            System.out.println("Недостаточно аргументов");
            System.exit(0);
        }
        TimeUnit timeUnit = TimeUnit.NANOSECONDS;
        final long beginning = System.nanoTime();

        ThreadNIOCopy thread = new ThreadNIOCopy(args[0],args[1]);
        ThreadNIOCopy threadTwo = new ThreadNIOCopy(args[0],args[2]);
        thread.run();
        threadTwo.run();

        final long end = System.nanoTime();
        System.out.printf("Время главного потока main = %d%n",
                timeUnit.toMillis(end - beginning));
    }
}
