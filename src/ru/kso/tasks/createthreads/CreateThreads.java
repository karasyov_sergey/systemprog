package ru.kso.tasks.createthreads;

/**
 * Класс, создающий 10 потоков
 */
public class CreateThreads {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new NewThread().start();
        }
    }
}