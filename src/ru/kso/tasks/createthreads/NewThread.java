package ru.kso.tasks.createthreads;

/**
 * Класс наследник Thread, предназначенный для создания новых потоков
 *
 * @author KSO 17ИТ17
 */
public class NewThread extends Thread {

    @Override
    public void run() {
        System.out.println("запуск потока " + getName());
    }
}

