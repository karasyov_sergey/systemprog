package ru.kso.tasks.rabbitnturtle;

import ru.kso.threads.AnimalThread;

public class RabbitNTurtle {
    public static void main(String[] args) {
        new AnimalThread("Черепашка", 10).start();
        new AnimalThread("Кролик", 1).start();
        System.out.println("Main завершился");
    }
}
