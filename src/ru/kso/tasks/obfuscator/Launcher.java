package ru.kso.tasks.obfuscator;

import java.io.IOException;
import java.util.Scanner;

/**
 * Лаунчер для обфускатора
 *
 * @author KSO 17ИТ17
 */
public class Launcher {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        try {
            System.out.println("Введите путь к файлу .java" +
                    ", который находится в вашем проекте");
            String path = scanner.nextLine();
            Obfuscator obfuscator = new Obfuscator(
                    path);
            obfuscator.obfuscate();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
    }
}
