package ru.kso.tasks.obfuscator;

import org.apache.bcel.Repository;
import org.apache.bcel.classfile.*;
import ru.kso.readers.strings.FReaderS;
import ru.kso.writers.strings.FWriterS;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, экземпляр которого является
 * обфускатором кода
 *
 * @author KSO 17ИТ17
 */
class Obfuscator {

    private JavaClass javaClass;
    private ClassNameGenerator classNameGenerator = new ClassNameGenerator();
    private String newClassName = classNameGenerator.generateClassName();
    private String classData;
    private String path;

    Obfuscator(String path) throws ClassNotFoundException {
        this.path = path;
        this.classData = readFromPath(path);
        this.javaClass = getJavaClass();
    }

    /**
     * Метод, который считывает и
     * возвращает данные, записанные
     * в java классе
     *
     * @param path путь к java файлу
     * @return содержимое java файла
     */
    private String readFromPath(String path) {
        FReaderS fReaderS = new FReaderS(path);
        fReaderS.toRead();
        return fReaderS.getDataText();
    }

    /**
     * Метод, который возвращает объект
     * типа JavaClass
     *
     * @return экземпляр класса JavaClass
     * @throws ClassNotFoundException исключение, выбрасываемое
     *                                в том случае, если класс не обнаружен
     */
    private JavaClass getJavaClass() throws ClassNotFoundException {
        String classPackage = getClassPackage();
        return Repository.lookupClass(classPackage);

    }

    /**
     * Метод, который ищет в содержимом
     * java файле имя пакета  и возвращает
     * полное имя пакета
     *
     * @return полное имя пакета
     */
    private String getClassPackage() {
        Pattern pattern = Pattern.compile("(?<=package).+?(?=;)");
        Matcher matcher = pattern.matcher(classData);
        String classPackage = "";
        if (matcher.find()) {
            classPackage = matcher.group().trim();
        }
        return classPackage +
                "." +
                new File(path).getName().
                        replace(".java", "");
    }

    /**
     * Метод, который обфусцирует
     * java код
     *
     * @throws IOException исключение, выбрасываемое
     *                     в случае ошибки при создании файла
     */
    void obfuscate() throws IOException {
        deleteSingleLineDoc();
        deleteSpace();
        deleteMultiLineDoc();
        setNewClassName();
        ArrayList<String> identifiers = getIdentifiersOfClass();
        setVariableNames(identifiers);
        createNewJavaFile();
    }

    /**
     * Метод, который удаляет все однострочные
     * комментарии в java коде
     */
    private void deleteSingleLineDoc() {
        classData = classData.replaceAll("//.+?//", "");
        classData = classData.replaceAll("//.+?\\n", "");

    }

    /**
     * Метод, который удаляет все
     * многострочные комментарии в java коде при
     * условии, что удалены все переходы
     * на новую строку.
     */
    private void deleteMultiLineDoc() {
        classData = classData.replaceAll("/\\*\\*.+?\\*/", "");
        classData = classData.replaceAll("/\\*.+?\\*/", "");
    }

    /**
     * Метод, который создает новый java class
     * и записывает в него обфусцированный код
     *
     * @throws IOException исключение, выбрасываемое
     *                     в случае ошибки при создании файла
     */
    private void createNewJavaFile() throws IOException {
        Path path = Paths.get("src/ru/kso/assets/obfuscator/" + newClassName + ".java");
        Files.createFile(path);
        FWriterS fWriterS = new FWriterS(path.toString(), classData);
        fWriterS.write();
    }

    /**
     * Метод, который устанавливает новое имя
     * классу java кода. Если класс с сгенерированным именем
     * существует то будет создано новое имя класса
     */
    private void setNewClassName() {
        File folder = new File("src/ru/kso/assets/obfuscator");
        String[] classes = folder.list((dir, name) -> name.endsWith(".java"));
        if (classes != null) {
            for (String className : classes){
                if (newClassName.equals(className.replace(".java",""))){
                    newClassName = classNameGenerator.generateClassName();
                }
            }
        }
        classData = classData.replaceAll(javaClass.getSourceFileName().
                        replace(".java", ""),
                newClassName);
    }

    private void deleteSpace() {
        classData = classData.replaceAll("\\s+", " ");
    }

    /**
     * Метод, который устанавливает новые
     * идентификаторы переменных java кода
     *
     * @param arrayList список всех идентификаторов
     */
    private void setVariableNames(ArrayList<String> arrayList) {
        IDGenerator IDGenerator = new IDGenerator();
        for (String element : arrayList) {
            classData = classData.replaceAll("(?<=[\\[\\] (.+!])" + element + "(?=[ \\[\\];=).,+])",
                    IDGenerator.generateIdentifier());


        }
    }

    /**
     * Метод, который ищет в java коде
     * все идентификаторы объектов
     *
     * @return списочный массив, содержащий
     * все идентификаторы объектов
     */
    private ArrayList<String> getIdentifiersOfClass() {
        ArrayList<String> tempList = new ArrayList<>();
        Method[] methods = javaClass.getMethods();
        Field[] fields = javaClass.getFields();
        LocalVariableTable[] localVariableTables = new LocalVariableTable[methods.length];
        for (int i = 0; i < methods.length; i++) {
            localVariableTables[i] = methods[i].getLocalVariableTable();
        }
        for (LocalVariableTable localVariableTable : localVariableTables) {
            LocalVariable[] temp = localVariableTable.getLocalVariableTable();
            for (LocalVariable variable : temp) {
                tempList.add(variable.getName());
            }
        }
        for (Field field : fields) {
            tempList.add(field.getName());
        }
        tempList.removeAll(Collections.singleton("this"));
        ArrayList<String> finallyList = new ArrayList<>();
        while (!tempList.isEmpty()) {
            finallyList.add(tempList.get(0));
            tempList.removeAll(Collections.singleton(tempList.get(0)));
        }

        return finallyList;

    }

}
