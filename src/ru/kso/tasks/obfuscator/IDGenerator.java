package ru.kso.tasks.obfuscator;

import java.util.ArrayList;

class IDGenerator {
    private static final int INITIAL_INDEX = 97;
    private static final int LAST_INDEX = 122;
    private int count = 0;
    private ArrayList<Integer> chars;

    IDGenerator() {
        this.chars = new ArrayList<>();
    }

    String generateIdentifier() {
        StringBuilder stringBuilder = new StringBuilder();
        if (count + INITIAL_INDEX > LAST_INDEX) {
            count = 0;
        }
        chars.add(INITIAL_INDEX + count);
        for (int symbolCode : chars) {
            stringBuilder.append((char) (symbolCode));
        }
        count++;
        return stringBuilder.toString();
    }
}
