package ru.kso.tasks.obfuscator;

import java.util.ArrayList;

class ClassNameGenerator {
    private static final int INITIAL_INDEX = 65;
    private static final int LAST_INDEX = 90;
    private int count = 0;
    private ArrayList<Integer> chars;

    ClassNameGenerator() {
        this.chars = new ArrayList<>();
        chars.add(INITIAL_INDEX);
    }

    String generateClassName() {
        StringBuilder stringBuilder = new StringBuilder();
        if (count + INITIAL_INDEX > LAST_INDEX) {
            count = 0;
            chars.add(INITIAL_INDEX + count);
        }
        for (Integer symbolCode : chars) {
            stringBuilder.append((char) (symbolCode + count));
        }
        count++;
        return stringBuilder.toString();
    }
}
