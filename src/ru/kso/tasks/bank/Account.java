package ru.kso.tasks.bank;

import ru.kso.myexceptions.bank.NegativeAmountException;
import ru.kso.myexceptions.bank.SumExcessException;

/**
 * Класс, экземпляр которого является
 * банковским аккаунтом
 *
 * @author KSO 17ИТ17
 */
@SuppressWarnings("all")
public class Account {
    private Long balance;
    private final Object waitObj = new Object();
    private BalanceState balanceState = BalanceState.INITIAL;

    public Account(Long balance) {
        this.balance = balance;
    }

    public Account() {
        this.balance = 0L;
    }

    public Long getBalance() {
        return balance;
    }

    /**
     * Метод, который пополняет баланс
     * на сумму, пришедшей извне
     *
     * @param amount сумма для пополнения
     * @throws NegativeAmountException исключение, выбрасываемое
     *                                 при получении извне
     *                                 отрицательного значения
     */
    public void deposit(Long amount) throws NegativeAmountException {
        synchronized (waitObj) {
            if (isCorrectAmount(amount)) {
                balance += amount;
                waitObj.notify();
            } else {
                throw new NegativeAmountException();
            }
        }
    }

    /**
     * Метод, который снимает необходимую сумму
     * денег с баланса
     *
     * @param amount сумма для снятия
     * @throws NegativeAmountException исключение, выбрасываемое
     *                                 при получении извне
     *                                 отрицательного значения
     */
    public void withdraw(Long amount) throws NegativeAmountException, SumExcessException {
        synchronized (waitObj) {
            if (isCorrectAmount(amount)) {
                while (balance < amount) {
                    if (balanceState.equals(BalanceState.UPDATE) ||
                            balanceState.equals(BalanceState.INITIAL)) {
                        try {
                            waitObj.wait(5000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        throw new SumExcessException();
                    }
                }
                balance -= amount;
            } else {
                throw new NegativeAmountException();
            }
        }
    }

    /**
     * Метод, проверяющий корректность
     * пришедших извне данных
     *
     * @param amount данные для пополнения / снятие
     * @return
     */
    private boolean isCorrectAmount(Long amount) {
        return amount > 0;
    }

    public void setBalanceState(BalanceState balanceState) {
        this.balanceState = balanceState;
    }

    /**
     * Вложенный класс типа перечисление, который
     * отражает текущее состояние баланса
     *
     * <li>баланс обновляется</li>
     * <li>баланс не обновляется</li>
     * <li>баланс инициализирован</li>
     */
    public enum BalanceState {
        UPDATE,
        NOT_UPDATE,
        INITIAL
    }

}