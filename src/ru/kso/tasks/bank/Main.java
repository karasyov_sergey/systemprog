package ru.kso.tasks.bank;

import ru.kso.myexceptions.bank.NegativeAmountException;
import ru.kso.myexceptions.bank.SumExcessException;
import ru.kso.threads.DepositRunnable;

/**
 * Запуск задачи bank на выполнение
 *
 * @author KSO 17ИТ17
 */
public class Main {
    public static void main(String[] args) {
        Account account = new Account();
        Thread thread = new Thread(new DepositRunnable(account, 100_000L));
        thread.start();
        try {
            account.withdraw(50_000L);
        } catch (NegativeAmountException | SumExcessException e) {
            e.printMessage();
        }
        System.out.printf("Баланс в текущий момент =  %d" +
                " копеек%n", account.getBalance());
    }
}
