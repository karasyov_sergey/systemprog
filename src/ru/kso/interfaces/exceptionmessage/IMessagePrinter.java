package ru.kso.interfaces.exceptionmessage;

public interface IMessagePrinter {
    void printMessage();
}
