package ru.kso.lessons.withoutloseresult;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        RandomRunExample.example();
        SeriesRunExample.example();
        (new InterferenceExample()).example();
    }
}
