package ru.kso.lessons.withoutloseresult;

public class StateObject {
    private int i;

    synchronized void increment(){
        i++;
    }

    public int getI(){
        return i;
    }
}