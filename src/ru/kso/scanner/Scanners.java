package ru.kso.scanner;

import java.util.Scanner;

/**
 * Класс, содержащий методы, для ввода информации с клавиатуры
 *
 * @author KSO 17ИТ17
 */
@SuppressWarnings("unused")
public class Scanners {
    private static Scanner scanner = new Scanner(System.in);

    /**
     * Метод, предназначенный для ввода информации с клавиатуры типа int
     *
     * @return данные типа int
     */
    public static int scannerInt() {
        return scanner.nextInt();
    }

    /**
     * Метод, предназначенный для ввода информации с клавиатуры типа double
     *
     * @return данные типа double
     */
    public static double scannerDouble() {
        return scanner.nextDouble();
    }

    /**
     * Метод, предназначенный для ввода информации с клавиатуры типа String
     * до первого пробела
     *
     * @return данные типа String
     */
    public static String scannerNext() {
        return scanner.next();
    }

    /**
     * Метод, предназначенный для ввода информации с клавиатуры типа String
     *
     * @return данные типа String
     */
    public static String scannerNextLine() {
        return scanner.nextLine();
    }
}
