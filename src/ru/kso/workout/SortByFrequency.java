package ru.kso.workout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Класс, сортирующий массив с числами по частоте их повторений (от большей
 * к меньшей)
 *
 * @author KSO 17ИТ17
 */
public class SortByFrequency {
    public static void main(String[] args) {
        ArrayList<Integer> inputArr = new ArrayList<>(Arrays.asList(2, 5, 2, 6, -1, 9999999, 5, 8, 8, 8));
        // объявление переменной, содержащая количество уникальных значений в списочном массиве inputArr
        int qOfUniqueNum = calcUniqueNum(inputArr);
        int[][] matrix = new int[qOfUniqueNum][2];
        toFillMatrix(matrix, inputArr);
        bubblySort(matrix);
        ArrayList<Integer> outputArr = toFillByFreq(matrix);
        System.out.println(outputArr);

    }

    /**
     * Метод calculation of unique numbers для
     * расчета количества уникальных чисел в списочном массиве
     *
     * @param integerArrList списочный массив, содержащий целые числа
     * @return количество уникальных чисел
     */
    private static int calcUniqueNum(ArrayList<Integer> integerArrList) {
        ArrayList<Integer> temp = new ArrayList<>(integerArrList);
        int quantityOfUniqueNumbers = 0;
        while (!temp.isEmpty()) {
            temp.removeAll(Collections.singleton(temp.get(0)));
            quantityOfUniqueNumbers++;

        }
        return quantityOfUniqueNumbers;
    }

    /**
     * Заполнение двумерного массива данными списочного массива по типу :
     * 1-й элмент строки - количество повторений уникального числа
     * 2-й элемент строки - число из списочного массива
     *
     * @param integerMatrix  двумерный массив типа данных int
     * @param integerArrList списочный массив, содержащий целые числа
     */
    private static void toFillMatrix(int[][] integerMatrix, ArrayList<Integer> integerArrList) {
        ArrayList<Integer> temp = new ArrayList<>(integerArrList);
        int i = 0;
        while (!temp.isEmpty()) {
            integerMatrix[i][0] = calcOfFreq(temp);
            integerMatrix[i][1] = temp.get(0);
            i++;
            temp.removeAll(Collections.singleton(temp.get(0)));
        }
    }

    /**
     * Метод для рассчета количества повторений первого элемента массива
     *
     * @param integerArrList списочный массив, содержащий целые числа
     * @return количество повторений первого элемента массива
     */
    private static int calcOfFreq(ArrayList<Integer> integerArrList) {
        int quantity = 1;
        for (int index = 1; index < integerArrList.size(); index++) {
            if (integerArrList.get(0).equals(integerArrList.get(index))) {
                quantity++;
            }
        }
        return quantity;
    }

    /**
     * Метод, сортирующий матрицу по первому элементу строки
     *
     * @param integerMatrix двумерный массив, содержащий целые числа
     */
    private static void bubblySort(int[][] integerMatrix) {
        for (int out = integerMatrix.length - 1; out >= 0; out--) {
            for (int index = 0; index < out; index++) {
                if (integerMatrix[index][0] < integerMatrix[index + 1][0]) {
                    int[] temp = integerMatrix[index];
                    integerMatrix[index] = integerMatrix[index + 1];
                    integerMatrix[index + 1] = temp;
                }
            }
        }
    }


    /**
     * Метод, заполняющий списочный массив числами по убыванию частоты
     *
     * @param integerMatrix двумерный массив, содержащий целые числа
     * @return списочный массив, содержащий числа по убыванию частоты
     */
    private static ArrayList<Integer> toFillByFreq(int[][] integerMatrix) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int[] line : integerMatrix) {
            for (int index = 0; index < line[0]; index++) {
                arrayList.add(line[1]);
            }
        }
        return arrayList;
    }
}
