package ru.kso.workout;

/**
 * Класс для рассчета количества шоколадок, которые могут быть куплены
 * покупателем, и дополнительных, выдаваемые за определенное количество оберток
 *
 * @author KSO 17ИТ17
 */
public class Chocolates {

    public static void main(String[] args) {
        int money = 15;
        int price = 1;
        int wrap = 3;
        int quantityOfChocolates = money / price;
        int addChocolates = calcAddChocolates(quantityOfChocolates, wrap);
        System.out.printf("Количество шоколадок = %d", quantityOfChocolates + addChocolates);
    }

    /**
     * Метод для рассчета количества дополнительных шоколадок, на получение которых
     * может рассчитывать покупатель
     *
     * @param quantityOfChocolates количество шоколадок
     * @param wrap                 количество оберток, требуемых для получения одной дополнительной шоколадки
     * @return общее количество дополнительных шоколадок
     */
    private static int calcAddChocolates(int quantityOfChocolates, int wrap) {
        if (quantityOfChocolates < wrap) {
            return 0;
        }
        return calcAddChocolates(quantityOfChocolates / wrap, wrap)
                + quantityOfChocolates / wrap;
    }
}
