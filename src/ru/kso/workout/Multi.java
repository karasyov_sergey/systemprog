package ru.kso.workout;

/**
 * Класс, умножающий два числа рекурсивным методом
 *
 * @author KSO 17ИТ17
 */
public class Multi {
    public static void main(String[] args) {
        int firstNumber = 5;
        int secondNumber = 8;
        int result = multi(firstNumber, secondNumber);
        System.out.println(result);
    }

    /**
     * Метод, умножающий два целых числа
     *
     * @param firstNumber  первое целое число
     * @param secondNumber второе целое число
     * @return результат умножения
     */
    static int multi(int firstNumber, int secondNumber) {
        if (firstNumber == 0 || secondNumber == 0) {
            return 0;
        }
        if (secondNumber < 0) {
            return multi(firstNumber, secondNumber + 1) - firstNumber;
        }
        return multi(firstNumber, secondNumber - 1) + firstNumber;
    }
}
