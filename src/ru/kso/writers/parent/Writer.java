package ru.kso.writers.parent;

/**
 * Класс, экземпляр которого записывает данные на файл
 *
 * @author KSO 17ИТ17
 */
@SuppressWarnings("unused")
public abstract class Writer {
    private String path;


    public Writer(String path) {
        this.path = path;
    }

    public abstract void write();

    protected String getPath() {
        return path;
    }

}
