package ru.kso.writers.strings;

import ru.kso.writers.parent.Writer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

@SuppressWarnings("unused")
public class FWriterS extends Writer {

    private String recordableData;

    public FWriterS(String path, String recordableData) {
        super(path);
        this.recordableData = recordableData;
    }

    private String getRecordableData() {
        return recordableData;
    }

    @Override
    public void write() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(getPath()))) {
            writer.write(getRecordableData());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
