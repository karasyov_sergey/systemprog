package ru.kso.writers.arrays;

import ru.kso.writers.parent.Writer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

@SuppressWarnings("unused")
public class FWriterA extends Writer {

    private String[] recordableData;

    public FWriterA(String path, String[] recordableData) {
        super(path);
        this.recordableData = recordableData;
    }

    private String[] getRecordableData() {
        return recordableData;
    }

    @Override
    public void write() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(getPath()))) {
            String[] data = getRecordableData();
            for (int i = 0; i < data.length; i++) {
                writer.write(data[i]);
                if (i != data.length - 1){
                    writer.write("\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
