package ru.kso.writers.arrlists;

import ru.kso.writers.parent.Writer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

@SuppressWarnings("unused")
public class FWriterAL extends Writer {

    private ArrayList<String> recordableData;

    public FWriterAL(String path, ArrayList<String> recordableData) {
        super(path);
        this.recordableData = recordableData;
    }

    private ArrayList<String> getRecordableData() {
        return recordableData;
    }

    @Override
    public void write() {
        try (BufferedWriter writer = new BufferedWriter(
                new FileWriter(getPath()))) {
            ArrayList<String> list = getRecordableData();
            for (int i = 0; i < list.size(); i++) {
                writer.write(list.get(i));
                if (i != list.size() - 1) {
                    writer.write("\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
