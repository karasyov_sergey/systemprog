package ru.kso.threads;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("unused")
public class ThreadNIOCopy extends Thread {
    private String pathSource;
    private String pathTarget;

    public ThreadNIOCopy(String pathSource, String pathTarget) {
        this.pathSource = pathSource;
        this.pathTarget = pathTarget;
    }

    private String getPathSource() {
        return pathSource;
    }

    private String getPathTarget() {
        return pathTarget;
    }

    @Override
    public void run() {
        TimeUnit timeUnit = TimeUnit.NANOSECONDS;
        final long beginning = System.nanoTime();
        try {
            Files.copy(Paths.get(getPathSource()), Paths.get(getPathTarget()),
                    StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
        final long end = System.nanoTime();
        System.out.printf("Время работы потока %s = %d%n", getName(),
                timeUnit.toMillis(end - beginning));
    }
}
