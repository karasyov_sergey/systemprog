package ru.kso.threads;

import ru.kso.writers.strings.FWriterS;

import java.util.concurrent.TimeUnit;

/**
 * Класс наследник Thread, который записывает данные на файл
 *
 * @author KSO 17ИТ17
 */
public class ThreadWriteFile extends Thread {
    private int threadPriority;
    private String pathFile;
    private String data;

    public ThreadWriteFile(String pathFile, String data) {
        this.pathFile = pathFile;
        this.data = data;
    }


    @Override
    public void run() {
        TimeUnit timeUnit = TimeUnit.NANOSECONDS;
        final long beginning = System.nanoTime();
        FWriterS writerToFile = new FWriterS(getPathFile(),getData());
        writerToFile.write();
        final long end = System.nanoTime();
        System.out.printf("Время потока записи %s = %d%n", getName(), timeUnit.toMillis(end - beginning));
    }

    private String getPathFile() {
        return pathFile;
    }

    @Override
    public String toString() {
        return "ThreadWriteFile{" +
                "threadPriority=" + threadPriority +
                ", pathFile='" + pathFile + '\'' +
                '}';
    }

    private String getData() {
        return data;
    }
}
