package ru.kso.threads;

import ru.kso.myexceptions.bank.NegativeAmountException;
import ru.kso.tasks.bank.Account;

/**
 * Класс, реализующий интерфейс Runnable. Экземпляр
 * данного класса будет пополнять баланс
 * пришедшего извне банковского аккаунта на
 * сумму, пришедшую вместе с банковским аккаунтом
 *
 * @author KSO 17ИТ17
 */
public class DepositRunnable implements Runnable {
    private final Account account;
    private Long amount;

    public DepositRunnable(Account account, Long amount) {
        this.account = account;
        this.amount = amount;
    }

    @Override
    public void run() {
        if (amount > 0) {
            account.setBalanceState(Account.BalanceState.UPDATE);
            for (long i = 1; i <= amount; i++) {
                try {
                    account.deposit(1L);
                } catch (NegativeAmountException e) {
                    e.printMessage();
                }
            }
        } else {
            System.out.println("Сумма для пополнения" +
                    "должна быть больше 0");
        }
        account.setBalanceState(Account.BalanceState.NOT_UPDATE);
    }
}
