package ru.kso.threads;

import ru.kso.readers.strings.FReaderS;

import java.util.concurrent.TimeUnit;

/**
 * Класс наследник Thread, который считывает данные с файла
 *
 * @author KSO 17ИТ17
 */
public class ThreadReadFile extends Thread {
    private String pathFile;
    private String data;

    public ThreadReadFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public String getData() {
        return data;
    }

    @Override
    public void run() {
        TimeUnit timeUnit = TimeUnit.NANOSECONDS;
        final long beginning = System.nanoTime();
        FReaderS readerFromFile = new FReaderS(getPathFile());
        readerFromFile.toRead();
        setData(readerFromFile.getDataText());
        final long end = System.nanoTime();
        System.out.printf("Время потока чтения %s = %d%n", getName(), timeUnit.toMillis(end - beginning));
    }

    private String getPathFile() {
        return pathFile;
    }

    private void setData(String data) {
        this.data = data;
    }
}
