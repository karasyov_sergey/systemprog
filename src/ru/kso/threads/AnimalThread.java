package ru.kso.threads;

public class AnimalThread extends Thread {
    private String threadName;
    private int threadPriority;


    public AnimalThread(String threadName, int threadPriority) {
        this.threadName = threadName;
        this.threadPriority = threadPriority;
        setPriority(threadPriority);
        setName(threadName);
    }

    @Override
    public void run() {
        for (int i = 1; i < 1001; i++) {
            System.out.println("бежим " + i + " метров " + getThreadName());
        }
        if (getThreadPriority() == 1){
            setThreadPriority(10);
        } else if (getThreadPriority() == 10){
            setThreadPriority(1);
        }
        for (int i = 1001; i < 4001; i++){
            System.out.println("бежим " + i + " метров " + getThreadName());
        }

    }

    private void setThreadPriority(int threadPriority) {
        this.threadPriority = threadPriority;
        setPriority(threadPriority);
    }

    private String getThreadName() {
        return threadName;
    }

    private int getThreadPriority() {
        return threadPriority;
    }
}
