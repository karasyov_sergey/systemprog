package ru.kso.threads;

/**
 * Класс наследник Thread, который при старте выводит в консоль
 * имя потока
 *
 * @author KSO 17ИТ17
 */
public class MyThread extends Thread {
    private String threadName;
    private static final int MIN = 0;
    private static final int MAX = 5;

    public MyThread(String threadName) {
        this.threadName = threadName;
        setName(threadName);
    }

    private String getThreadName() {
        return threadName;
    }

    @Override
    public void run() {
        for (int i = MIN; i < MAX; i++) {
            try {
                System.out.println(getThreadName());
                sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}